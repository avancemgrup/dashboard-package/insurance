@extends('layouts.administration.master')

@section('site-title')
    Seguros
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('insurance.category.create')
                        <a href="{{ route('insurances-category-add') }}" class="btn btn-success">
                           Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-offset-6 col-lg-offset-6 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                {!! Form::open(array('route' => ['insurances-category'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Categoria de seguros</h3>
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Slug</th>
                            @if(Entrust::can('insurance.category.edit') || Entrust::can('insurance.category.delete'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($insurancesCategories as $insuranceCategory)
                            <tr>
                                <td>
                                    @if(Entrust::can('insurance.view') || Entrust::can('insurance.create') || Entrust::can('insurance.edit') || Entrust::can('insurance.delete'))
                                    <a class="card__insurance-name" href="{{ url('administration/insurances/'.$insuranceCategory->id) }}">{{$insuranceCategory->name}}</a>
                                    @else
                                    {{$insuranceCategory->name}}
                                    @endif
                                </td>
                                <td class="table-ellipsis">{{$insuranceCategory->description}}</td>
                                <td class="table-ellipsis">{{$insuranceCategory->slug}}</td>
                                @if($insuranceCategory->id != 1)
                                    <td>
                                        @permission('insurance.category.edit')
                                        <a href="{{ url('administration/insurances/edit/category/'.$insuranceCategory->id) }}" class="btn btn-default btn-icon">
                                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                        </a>
                                        @endpermission
                                        @permission('insurance.category.delete')
                                        {!! Form::open(array('route' => ['insurances-delete-category', $insuranceCategory->id], 'method'=>'DELETE', 'enctype' => 'multipart/form-data', 'style' => 'display:inline-block')) !!}
                                        <button class="btn btn-danger btn-icon"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}
                                        @endpermission
                                   </td>
                                @else
                                    <td></td><td></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                </div>
                <div class="row middle-xs end-md end-lg">
                    {{ $insurancesCategories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function deleteInsuranceCategory() {
            let checkbox = document.querySelectorAll('input[name=category]:checked');
            let category_id = [];
            for (let i = 0; i < checkbox.length; i++) {
                category_id.push(checkbox[i].getAttribute("data-id"));
            }
            console.log(category_id);

            $.ajax({
                url: "insurances/categories",
                type: 'DELETE',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'category_id': category_id
                },
                complete: function () {
                   // window.location.reload();
                }
            })
        }

        function selectAll() {
            let check = document.querySelectorAll("input[name=all]:checked").length;
            let checkbox = document.querySelectorAll('input[name=category]');

            for (let i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = check == 1 ? true : false;
            }

        }
        document.getElementById("removeBtn").addEventListener("click", deleteInsuranceCategory);
        document.querySelectorAll("input[name=all]")[0].addEventListener("change", selectAll);
    </script>
@endsection