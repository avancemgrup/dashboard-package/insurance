@extends('layouts.administration.master')

@section('site-title')
    Seguros
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('insurance.create')
                        <a href="{{ route('insurances-add') }}" class="btn btn-success">
                            Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-offset-6 col-lg-offset-6 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 {!! Form::open(array('route' => ['insurances'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Seguros</h3>
                    @if (Session::has('error'))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger">
                                        {{ Session::get('error')  }}
                                    </div>
                                </div>
                            </div>
                    @endif
                    @if (Session::has('message'))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-success">
                                        {{ Session::get('message')  }}
                                    </div>
                                </div>
                             </div>
                    @endif
                @if($insurances)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Slug</th>
                                <th>Descripción corta</th>
                                @if(Entrust::can('insurance.edit') || Entrust::can('insurance.delete'))
                                <th>Opciones</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($insurances as $insurance)
                                <tr>
                                    <td>
                                        @if(Entrust::can('insurance.category.*'))
                                        <a class="card__insurance-name" href="{{ url('administration/insurances/'.$insurance->id."/insurances-category") }}">{{$insurance->name}}</a>
                                        @else
                                        {{$insurance->name}}
                                        @endif
                                    </td>
                                    <td>{{$insurance->slug}}</td>
                                    <td class="table-ellipsis">{{$insurance->short_description}}</td>
                                    <td>
                                        @permission('insurance.edit')
                                        <a href="{{ url('administration/insurances/edit/'.$insurance->id)}}" class="btn btn-default btn-icon">
                                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                        </a>
                                        @endpermission
                                        @permission('insurance.delete')
                                        {!! Form::open(array('route' => ['insurances-delete', $insurance->id], 'method'=>'DELETE', 'enctype' => 'multipart/form-data', 'style' => 'display:inline-block')) !!}
                                        <button class="btn btn-danger btn-icon"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}
                                        @endpermission
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                    </div>
                    <div class="row middle-xs end-md end-lg">
                        {{ $insurances->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection