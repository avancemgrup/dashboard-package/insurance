@extends('layouts.administration.master')

@section('site-title')
    Seguros
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nueva Categoria</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('insurances-category') }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'insurances-category-add', 'method'=>'POST')) !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nombre</label>
                    {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control', 'id' => 'name')) !!}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="slug">Slug</label>
                    {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control')) !!}
                    <span class="text-danger">{{ $errors->first('slug') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control', 'id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <button type="submit" class="btn btn-success">
                    Crear
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function string_to_slug(e) {
            var str = e.target.value;
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            document.querySelector("input[name=slug]").value = str;
            return false;

        }
        document.querySelector("input[name=name]").addEventListener('input',string_to_slug);
    </script>
@endsection