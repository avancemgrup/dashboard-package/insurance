@extends('layouts.administration.master')

@section('site-title')
    Seguros
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nuevo Seguro</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('insurances') }}">
                     Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'insurances-add-post', 'method'=>'POST', 'enctype' => 'multipart/form-data')) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nombre</label>
                    {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control', 'id' => 'name')) !!}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="slug">Slug</label>
                    {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control', 'id' => 'slug')) !!}
                    <span class="text-danger">{{ $errors->first('slug') }}</span>
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Categoria</label><br/>
                    @foreach($insurancesCategories as $category)
                        {{ Form::checkbox('insuranceCategory[]', $category->id, null) }}&nbsp;{{$category->name}}<br>
                    @endforeach
                    <span class="text-danger">{{ $errors->first('insuranceCategory') }}</span>
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nivel de seguro</label><br/>
                    {{ Form::checkbox('top', '1', null) }}&nbsp;top<br>
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Icono</label><br/>
                    <select id="icon" class="form-control" name="icon" style="width: 100%">
                        @foreach($icons as $icon)
                        <option value="{{$icon['id']}}"><span><i class="fa fa-{{$icon['id']}}" aria-hidden="true"></i>{{ $icon['name'] }}</span></option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group {{ $errors->has('roimageute') ? 'has-error' : '' }}">
                    <label for="image">Imagen</label>
                    {!! Form::file('image', array('accept' => 'image/*','class' => 'form-control', 'id' => 'image')) !!}
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>
                <div class="form-group {{ $errors->has('short_description') ? 'has-error' : '' }}">
                    <label for="short_description">Descripción corta</label>
                    {!! Form::textarea('short_description', null, array('placeholder' => 'Descripción corta','class' => 'form-control', 'id' => 'short_description','rows'=>'3')) !!}
                    <span class="text-danger">{{ $errors->first('short_description') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control', 'id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <button type="submit" class="btn btn-success">
                    Crear
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset("js/tinymce/tinymce.min.js") }}"></script>
    <script src="{{ asset("js/tinymce/tinymce_editor.js") }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        editor_config.path_absolute = "{{ url('/') }}";
        editor_config.selector = "textarea[name=description]";
        tinymce.init(editor_config);

        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><i class="fa fa-'+state.element.value.toLowerCase()+'" aria-hidden="true"></i></span>&nbsp;'+state.text+'</span>'
            );
            return $state;
        };
        $("#icon").select2({
            templateResult: formatState
        });

        function string_to_slug(e) {
            var str = e.target.value;
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            document.querySelector("input[name=slug]").value = str;
            return false;

        }
        document.querySelector("input[name=name]").addEventListener('input',string_to_slug);


    </script>
@endsection