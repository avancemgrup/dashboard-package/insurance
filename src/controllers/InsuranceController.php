<?php

namespace Avannubo\Insurance\Controllers;

use Avannubo\Insurance\Models\Insurance;
use Avannubo\Insurance\Models\InsuranceCategory;
use Avannubo\Seo\Models\Seo;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;

class InsuranceController extends Controller{

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description this method show all the category
     */
    public function category(Request $request){
        $search = $request->input('search');
        $insurancesCategories = InsuranceCategory::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('insurances::categories', compact('insurancesCategories'));
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show all the insurances
     */
    public function  insurances(Request $request){
        $search = $request->input('search');
        $insurances = Insurance::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('insurances::insurances', compact('insurances'));
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show all the insurances of the category
     */
    public function  showInsurancesCategory($idCategory){
        $category = InsuranceCategory::find($idCategory);

        if($category){
            $insurances =$category->insurances()->paginate(15);
            if($insurances->total() != 0){
                return view('insurances::insurances', compact('insurances','idCategory'));
            }else{
                return redirect('administration/insurances/category')->with(
                    'error', 'Esta categoria no contiene ningun seguro'
                );
            }
        }
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show all the category of the insurance
     */
    public function  showCategoryInsurance($idInsurance){
        $insurance = Insurance::find($idInsurance);
        if($insurance){
            $insurancesCategories = $insurance->insuranceCategories()->paginate(15);
            if($insurancesCategories->total() !=0){
                return view('insurances::categories', compact('insurancesCategories'));
            }else{
                return redirect('administration/insurances/category')->with(
                    'error', 'Este seguro no contiene ninguna Categoria.'
                );
            }
        }

    }


    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method show the form of the category 
     */
    public function createCategory(){
        return view('insurances::form-category');
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method show the form of the insurance
     */
    public function createInsurances(){
        $path = public_path()."/font-awesome/Font-Awesome.json";
        $icons = json_decode(file_get_contents($path), true);
        $insurancesCategories = InsuranceCategory::all();
        return view('insurances::form-insurances' , compact('insurancesCategories','icons'));
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are
     * correct it inserts the category into the database
     * else Shows error
     *
     */
    public function storeCategory(Request $request){
        // Validation
        $request['slug'] = str_slug($request->get('slug'));
        $rules = [
            'name' => 'required|Max:255|unique:insurance_categories,name',
            'slug' => 'required|Max:255|unique:insurance_categories,slug',
            'description' => 'required|Max:400',
        ];
        $this->validate($request, $rules);

        $category = new InsuranceCategory([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'slug' => $request->input('slug'),
        ]);
        if($category->save()){
            return redirect('administration/insurances/category')->with(
                'message', 'Categoria creada correctamente'
            );
        }else{
            return redirect('administration/insurances/category')->with(
                'error', 'La categoria no se a creado correctamente,intentelo mas tarde'
            );
        }


    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are 
     * correct it inserts insurance into the database
     *
     */
    public function storeInsurance(Request $request){
        $request['slug'] = str_slug($request->get('slug'));
        $rules = [
            'name' => 'required|Max:255',
            'insuranceCategory' => 'required|array|exists:insurance_categories,id',
            'slug' => 'required|Max:255|unique:insurances,slug',
            'image' => 'required|image',
            'top' => 'nullable|Max:255',
            'icon' => 'nullable|Max:255',
            'short_description' => 'required|Max:400',
            'description' => 'required|Max:65000',
        ];
        $this->validate($request, $rules);

        $image = $request->file('image');
		$timeImage = time();
        $FileName = $timeImage.'.'.$image->getClientOriginalExtension();
        Storage::putFileAs('public/insurances', new File($image), $FileName );

        $insurance = new Insurance([
            'name' => $request->input('name'),
            'image' => $FileName,
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'short_description' => $request->input('short_description'),
            'top' => $request->input('top'),
            'icon' => $request->input('icon'),
        ]);
        $insurance->save();
        $insurance->insuranceCategories()->sync($request->input('insuranceCategory'));

        if(class_exists(Seo::class)){
			$img1 = $timeImage.'_1.'.$image->getClientOriginalExtension();
            $img2 = $timeImage.'_2.'.$image->getClientOriginalExtension();
            $img3 = $timeImage.'_3.'.$image->getClientOriginalExtension();

            Storage::putFileAs('public/seos', new File($image), $img1);
            Storage::putFileAs('public/seos', new File($image), $img2);
            Storage::putFileAs('public/seos', new File($image), $img3);
		
            $seos = new Seo([
                'title' => $request->input('name'),
                'author' => "insurance web",
                'description' => $request->input('short_description'),
                'keyword' => "",
                'robots' => "",
                'canonical_url' => "",
                'language' => "Spanish",
                'generator' => "Avannubo",
                'route' => route('sg.show.insurance', ['id' => $insurance->id, 'slug' => $request->input('slug')]),
                'og_title' => $request->input('name'),
                'og_description' => $request->input('short_description'),
                'og_type' => "article",
                'og_image' => $img1,
                'og_url' => route('sg.show.insurance', ['id' => $insurance->id, 'slug' => $request->input('slug')]),
                'og_site_name' => config('app.name'),
                'tw_card' => "summary",
                'tw_url' => route('sg.show.insurance', ['id' => $insurance->id, 'slug' => $request->input('slug')]),
                'tw_title' => $request->input('name'),
                'tw_description' => $request->input('short_description'),
                'tw_image' => $img2,
                'gl_page_type' => "article",
                'gl_name' => $request->input('name'),
                'gl_description' => $request->input('short_description'),
                'gl_image' => $img3,
            ]);
			
            $seos->save();
        }

        return redirect()->route('insurances')->with(
            'message', 'Seguro creado correctamente'
        );
    }


    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method delete the informacion in the database
     */
    public function deleteCategory ($idCategory){
        // Validation
        if ($idCategory == 1) {
            return redirect()->route('insurances-category')->with(
                'error','No se puede eliminar la categoría "Sin categoría"'
            );
        }
        $insurancesCategory = InsuranceCategory::find($idCategory);
        if($insurancesCategory){
            foreach ($insurancesCategory->insurances as $insurance) {
                if ($insurance->insuranceCategories()->count() == 1) {
                    $insurance->insuranceCategories()->attach(InsuranceCategory::first());
                    $insurance->save();

                }
            }
            if(!$insurancesCategory->delete()){
                Session::flash('error','Categoria no eliminada correctamente, intentelo mas tarde');
            }else{
                Session::flash('message','Categoria eliminada correctamente');
            }
        }else{
            Session::flash('error','Categoria no encontrada');
        }
        return redirect()->route('insurances-category');
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method delete the informacion in the database
     */
    public function insuranceDelete ($idInsurance){
        // Validation
        $insurances = Insurance::find($idInsurance);
        if($insurances){
            if($insurances->delete()){
                if (Storage::disk('public')->exists('insurances/' . $insurances->image)) {
                    Storage::disk('public')->delete('insurances/' . $insurances->image);
            
                }
                Session::flash('message','Seguro eliminado correctamente');
            }else{
                Session::flash('error','Seguro no eliminado correctamente, intentelo mas tarde');
            }
        }else{
            Session::flash('error','Seguro no encontrado');
        }
        return redirect()->route('insurances');
    }


    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the category in 
     * the database,
     * if it not existe redirect to index else show the update form category
     */
    public function editCategory($idCategory){
        $insuranceCategory = InsuranceCategory::find($idCategory);
        if($insuranceCategory){
            return view('insurances::updateForm-category',compact('insuranceCategory','idCategory'));
        }
        return redirect()->route('insurances')->with(
            'error','Categoria de seguros no encontrada'
        );
    }

    /**
     * @author: Obiang
     * @date: 30/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the isnsurance  category in
     * the database,
     * if it not existe redirect to index else show the update form insurance
     */
    public function editInsurance($idInsurance){
        //$icons = Insurance::all();
        $insurance = Insurance::find($idInsurance);
        //dd($insurance->insuranceCategories()->select('insurance_category_id')->get());
        $categories = InsuranceCategory::all();
        $idCategories = [];
        $idInsuranceIcon[] = $idInsurance;
        $insuranceCategory = null;

        $path = public_path()."/font-awesome/Font-Awesome.json";
        $icons = json_decode(file_get_contents($path), true);
        if($insurance){
            foreach ($categories as $cat){
                if($insurance->insuranceCategories()->find($cat->id)){
                    $insuranceCategory = $insurance->insuranceCategories()->find($cat->id);
                    $idCategories[] =  $cat->id;
                }
            }
            if($insuranceCategory){
                return view('insurances::updateForm-insurance',compact('insurance','idInsurance','categories','idCategories','icons','idInsuranceIcon'));
            }
            return redirect()->route('insurances')->with(
               'error','Categoria no encontrada'
            );
        }
        return redirect()->route('insurances')->with(
            'error','Seguro no encontrado'
        );
    }


    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it update it into the database
     * else Shows error
     */
    public function updateCategory(Request $request, $id){
        // Validation
        $request['slug'] = str_slug($request->get('slug'));
        if ($id == 1) {
            return redirect()->route('insurances-category')->with(
                'error','No hay categoría, no se puede actualizar'
            );
        }
        $category = InsuranceCategory::find($id);
        if($category){
            $rules = [
                'name' => "required|Max:255|unique:insurance_categories,name,$id",
                'slug' => 'required|Max:255|unique:insurance_categories,slug,'.$id,
                'description' => 'required|Max:400',
            ];
            $this->validate($request, $rules);

            $category->name = $request->input('name');
            $category->description = $request->input('description');
            $category->slug = $request->input('slug');
            if($category->update()){
                return redirect()->route('insurances-category')->with(
                    'message', 'Categoria actualizada correctamente'
                );
            }else{
                return redirect()->route('insurances-category')->with(
                    'message', 'La categoria no se a actualizado correctamente,intentelo mas tarde'
                );
            }
        }
        return redirect()->route('insurances-category')->with(
            'error', 'Categoria no encontrada'
        );
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it update it into the database
     * else Shows error
     */
    public function updateInsurance(Request $request, $idInsurance){
        // Validation
        $request['slug'] = str_slug($request->get('slug'));
        $insurances = Insurance::find($idInsurance);
        $category = $request->input('insuranceCategory');

        if($category == null){
            return redirect()->route('insurances')->with(
               'error','Seguro no encontrado'
            );
        }
        if($insurances){
            $rules = [
                'name' => 'required|Max:255',
                'insuranceCategory' => 'required|array|exists:insurance_categories,id',
                'slug' => 'required|Max:255|unique:insurances,slug,' . $idInsurance,
                'image' => 'image',
                'top' => 'nullable|Max:255',
                'icon' => 'nullable|Max:255',
                'short_description' => 'required|Max:400',
                'description' => 'required|Max:65000',
            ];
            $this->validate($request, $rules);

            if($request->file('image') != null){
                if (Storage::disk('public')->exists('insurances/'.$insurances->image)) {
                    Storage::disk('public')->delete('insurances/'.$insurances->image);
                }
                $image = $request->file('image');
                $fileName = time(). '.' .$image->getClientOriginalExtension();
                Storage::putFileAs('public/insurances', new File($image), $fileName );
                $insurances->image = $fileName;
            }

            $insurances->name = $request->input('name');
            $insurances->short_description = $request->input('short_description');
            $insurances->description = $request->input('description');
            $insurances->slug = $request->input('slug');
            $insurances->top = $request->input('top');
            $insurances->icon = $request->input('icon');

            $insurances->update();
            $insurances->insuranceCategories()->detach();
            for($i=0; $i<count($category); $i++){
                $insurancesCategory = InsuranceCategory::find($category[$i]);
                $insurances->insuranceCategories()->attach($insurancesCategory->id);
            }
            if($insurances){
                return redirect()->route('insurances')->with(
                    'message', 'Seguro actualizado correctamente'
                );

            }else{
                return redirect()->route('insurances')->with(
                    'message', 'El seguro no se a actualizado correctamente,intentelo mas tarde'
                );
            }

        }
        return redirect()->route('insurances')->with(
            'error', 'Seguro no encontrado'
        );
    }

}
