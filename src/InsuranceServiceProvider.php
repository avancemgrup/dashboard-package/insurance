<?php

namespace Avannubo\Insurance;
use Illuminate\Support\ServiceProvider;


class InsuranceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/Insurance.php';
        include __DIR__.'/models/InsuranceCategory.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'insurances');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //register controller
        $this->app->make('Avannubo\Insurance\Controllers\InsuranceController');

    }
}
