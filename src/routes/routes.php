<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {

    Route::get('insurances', [
        'as' => 'insurances',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@insurances',
       'middleware' => ['permission:insurance.view|insurance.create|insurance.edit|insurance.delete']
    ]);
    Route::get('insurances/category', [
        'as' => 'insurances-category',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@category',
        'middleware' => ['permission:insurance.category.create|insurance.category.edit|insurance.category.delete']
    ]);
    Route::get('insurances/{idCategoy}', [
        'as' => 'insurances-list-category',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@showInsurancesCategory',
        'middleware' => ['permission:insurance.category.create|insurance.category.edit|insurance.category.delete']
    ]);
    Route::get('insurances/{idInsurances}/insurances-category', [
        'as' => 'insurances-insurance-category\'',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@showCategoryInsurance',
        'middleware' => ['permission:insurance.view|insurance.create|insurance.edit|insurance.delete']
    ]);
    Route::get('insurances/category/add', [
        'as' => 'insurances-category-add',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@createCategory',
        'middleware' => ['permission:insurance.category.create']
    ]);
    Route::get('insurances/form/add', [
        'as' => 'insurances-add',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@createInsurances',
        'middleware' => ['permission:insurance.create']
    ]);
    Route::get('insurances/edit/category/{idCategory}',[
        'as' => 'insurances-edit-category',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@editCategory',
        'middleware' => ['permission:insurance.category.edit']
    ]);
    Route::get('insurances/edit/{idInsurance}',[
        'as' => 'insurances-edit',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@editInsurance',
        'middleware' => ['permission:insurance.edit']
    ]);
    Route::post('insurances/add', [
        'as' => 'insurances-add-post',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@storeInsurance',
        'middleware' => ['permission:insurance.create']
    ]);
    Route::post('insurances/category/add',[
        'as' => 'insurances-category-add',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@storeCategory',
        'middleware' => ['permission:insurance.category.create']
    ]);
    Route::put('insurances/category/{idCategory}',[
        'as' => 'insurances-edit-category',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@updateCategory',
        'middleware' => ['permission:insurance.category.edit']
    ]);
    Route::put('insurances/{idInsurance}',[
        'as' => 'insurances-edit',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@updateInsurance',
        'middleware' => ['permission:insurance.edit']
    ]);
    Route::delete('insurances/{idInsurance}',[
        'as' => 'insurances-delete',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@insuranceDelete',
        'middleware' => ['permission:insurance.delete']
    ]);
    Route::delete('insurances/category/{idInsurance}',[
        'as' => 'insurances-delete-category',
        'uses' => '\Avannubo\Insurance\Controllers\InsuranceController@deleteCategory',
        'middleware' => ['permission:insurance.category.delete']
    ]);

});