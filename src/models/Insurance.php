<?php

namespace Avannubo\Insurance\Models;

use Illuminate\Database\Eloquent\Model;


class Insurance extends Model
{
    protected $fillable = [
        'id',
        'name',
        'slug',
        'short_description',
        'description',
        'image',
        'top',
        'icon',
    ];

    public function insuranceCategories(){
        return $this->belongsToMany(InsuranceCategory::class,'insurance_insurance_category');
    }
}
