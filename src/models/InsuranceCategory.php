<?php

namespace Avannubo\Insurance\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceCategory extends Model
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'slug'
    ];

    public function insurances(){
        return $this->belongsToMany(Insurance::class,'insurance_insurance_category');
    }
}
