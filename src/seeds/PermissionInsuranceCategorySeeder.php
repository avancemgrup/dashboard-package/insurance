<?php

namespace Avannubo\Insurance\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionInsuranceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'insurance.category.view',
            'display_name' => 'insurance category  view',
            'description' => 'Ver categoria de seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.category.create',
            'display_name' => 'insurance category create',
            'description' => 'Crear categoria de seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.category.edit',
            'display_name' => 'insurance category edit',
            'description' => 'Editar categoria de seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.category.delete',
            'display_name' => 'insurance category delete',
            'description' => 'Eiminar categoria de seguros'
        ]);
        $person->save();
    }
}
