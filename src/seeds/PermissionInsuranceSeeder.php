<?php
namespace Avannubo\Insurance\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionInsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'insurance.view',
            'display_name' => 'insurance  view',
            'description' => 'Ver seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.create',
            'display_name' => 'insurance create',
            'description' => 'Crear seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.edit',
            'display_name' => 'insurance edit',
            'description' => 'Editar seguros'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'insurance.delete',
            'display_name' => 'insurance delete',
            'description' => 'Eliminar seguros'
        ]);
        $person->save();
    }
}
