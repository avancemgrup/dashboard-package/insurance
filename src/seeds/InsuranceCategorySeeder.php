<?php

namespace Avannubo\Insurance\Seeds;
use Illuminate\Database\Seeder;
use Avannubo\Insurance\Models\InsuranceCategory;

class InsuranceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new InsuranceCategory([
            'id' => 1,
            'name' => 'No category',
            'description' => 'No category default',
            'slug'=>'no-category',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $category->save();
    }
}
