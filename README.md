## Avannubo package insurance

Insurance

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/insurance": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "https://gitlab.com/avancemgrup/dashboard-package/insurance.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Insurance\InsuranceServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Insurance\Seeds\InsuranceCategory`
* seeders: `php artisan db:seed --class=Avannubo\Insurance\Seeds\PermissionInsuranceSeeder`
* seeders: `php artisan db:seed --class=Avannubo\Insurance\Seeds\PermissionInsuranceCategorySeeder`
